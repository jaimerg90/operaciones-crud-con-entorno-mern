const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Elegir los datos y el tipo de datos de la entidad

let Usuario = new Schema({
    nombre: {
        type: String,
        //validate: /^[a-zA-Z0-9]+$/
    },
    email: {
        type: String,
        //validate: /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/
    },
    password: {
        type: String,
        minlength: 8,
        //validate: /^(?=.*\d)(?=.*[\u0021-\u002b\u003c-\u0040])(?=.*[A-Z])(?=.*[a-z])\S{8,16}$/
    }
});

module.exports = mongoose.model('Usuario', Usuario);