const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const Usuario = require('./modelo');

const app = express();
const PORT = 4000; //En mayúscula porque siempre va a ser igual

// Middleware para la serialización y deserialización (parseo) automática
app.use(bodyParser.json());
app.use(cors());

mongoose.connect('mongodb://127.0.0.1:27017/db_usuarios'); //Si no existe crea la bbdd y si existe se conecta
const conexion = mongoose.connection;
conexion.once('open', () => console.log('Se ha conectado con éxito'));


const rutasAPI = express.Router();

//Va a hacer de intermediario en la url /api/usuarios
app.use('/api/usuarios', rutasAPI);
//http://127.0.0.1:4000/api/usuarios/registro


rutasAPI.route('/registro').post((req, res) => {
    let nuevoUsuario = new Usuario(req.body);

    Usuario.findOne({email: nuevoUsuario.email}, (err, usuarioRegistrado) => {
        if(usuarioRegistrado){
            console.log('El usuario ya se encuentra registrado')
        } else {
            nuevoUsuario.save().then((usuario) => {
                res.status(200).json({
                    'usuario': 'Creado satisfactoriamente'
                })
            }).catch((err) => {
                res.status(400).send('El registro ha fallado');
            });
        }
    })
})

rutasAPI.route("/").get((req, res) => {
    Usuario.find((err, data) => {
        if(err){
            console.log(err);
        } else {
            res.json(data);
        }
    })
});

rutasAPI.route("/:id").get((req, res) => {
    let id = req.params.id;

    Usuario.findById(id, (err, data) => {
        if(err){
            console.log(err);
        } else {
            res.json(data)
        }
    })
})

rutasAPI.delete("/:id", (req, res) => {
    let id = req.params.id;
    Usuario.findByIdAndDelete(id, (err) => {
        if(err){
            console.log('FALLO');
        } else {
            res.json({
                mensaje: 'OK'
            })
        }
    })
})

rutasAPI.put("/:id", (req, res) => {
    let usuario = new Usuario(req.body);
    id = req.params.id;

    Usuario.findById(id, (err, user) => {
        for(const campo in req.body){
            console.log(user[campo])
            user[campo] = req.body[campo]
        }
        if(err){
            console.log(err);
        } else {
            user.save();
            res.json({
                mensaje: 'OK'
            })
        } 
    })
});

app.listen(PORT, () => console.log(`Servidor corriendo en el puerto ${PORT}`));