// Construir el componente raiz

import React from "react"; //Viene de la dependencia del package json
import {BrowserRouter as Router, Route, Link} from "react-router-dom";
import logo from "../logo.svg";
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";
import CrearUsuario from './CrearUsuario';
import ListarUsuarios from './ListarUsuarios';
import EditarUsuario from './EditarUsuario';


function App(){
    let estiloLogo = {
        width: "5em",
        height: "5em"
    }

    return (
        <Router>
            <div className="App">
                <header className="App-header">
                    <img src={ logo } style={ estiloLogo } className="App-logo" alt="logo"/>
                    <h1>Operaciones CRUD usuarios</h1>
                </header>
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <div class="collapse navbar-collapse">
                        <ul class="navbar-nav">
                            <li class="nav-item active">
                                <Link to="/" class="nav-link">Listado</Link>
                            </li>
                            <li class="nav-item">
                                <Link to="/registro" class="nav-link">Registrarse</Link>
                            </li>
                        </ul>
                    </div>
                </nav>
                <Route path="/" exact component={ListarUsuarios}/>
                <Route path="/registro" component={CrearUsuario}/>
                <Route path="/editar/:id" component={EditarUsuario}/>
            </div>
        </Router>
        );
}
export default App;