import React, {Component} from 'react';
import './formulario.css';


//Al importar el {Component} no hace falta poner React.Component al extender.
class CrearUsuario extends Component{
    //this.props es un objeto con datos públicos
    //this.props es un objeto con datos privados, es decir, el estado interno del componente. Como en angular las variables miembro de las clases privadas
    constructor(props){
        super(props);//Invoca al constructor del padre pasándole las propiedades públicas

        //Para evitar el problema del this con js. .bind() hace que cuando se invoque el evento, this sea realmente this (el propio componente) y no haya fallos.
        this.onChangeNombre = this.onChangeNombre.bind((this));
        this.onChangeEmail = this.onChangeEmail.bind((this));
        this.onChangePassword = this.onChangePassword.bind((this));
        this.onSubmit = this.onSubmit.bind((this));

        //Obj vacío para recibir los datos del form
        this.state = {
            nombre: '',
            email: '',
            password: ''
        }
    }

    onChangeNombre(evt){
        this.setState({
            nombre: evt.target.value
        })
    }
    // Método invocado por React cada vez que se cambia el valor del input y se envía un objeto con la información del evento
    onChangeEmail(evt){
        this.setState({
            email: evt.target.value
        });
    }

    onChangePassword(evt){
        this.setState({
            password: evt.target.value
        });
    }

    onSubmit(evt){
        evt.preventDefault();

        //Invocar el servicio (cliente HTTP, Ajax, fetch...)
        console.log(`Datos: ${this.state.nombre}, ${this.state.email}, ${this.state.password}`);
        window.fetch('http://127.0.0.1:4000/api/usuarios/registro', {
            method: 'post',
            body: JSON.stringify({
                nombre: this.state.nombre,
                email: this.state.email,
                password: this.state.password
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then((res) => {
            res.json().then((obj) => {
                if(obj.usuario === 'Creado satisfactoriamente'){
                    alert('Usuario creado con éxito');
                    window.location.href = "http://localhost:3000/";
                } else {
                    alert('No se ha podido crear el usuario')
                }
            })
        })
    }

    render(){
        return (
            <div>
                <h2>Crea un nuevo usuario</h2>
                <form onSubmit={this.onSubmit} data-id={this.state.id}>
                    <div class="form-group">
                        <label>Nombre</label>
                        <input type="email" class="form-control" type="text" onChange={this.onChangeNombre} value={this.state.nombre}/>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control" type="email" onChange={this.onChangeEmail} value={this.state.email}/>
                    </div>
                    <div class="form-group">
                        <label>Contraseña</label>
                        <input type="password" class="form-control" onChange={this.onChangePassword} value={this.state.password}/>
                    </div>
                    <input type="submit" class="btn btn-primary" value="Registrar"/>
                </form>
            </div>
        );
    }
}

export default CrearUsuario;