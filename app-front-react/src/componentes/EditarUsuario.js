import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Link} from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./formulario.css"

class EditarUsuario extends Component{
    constructor(props){
        super(props);
  
        this.onChangeNombre = this.onChangeNombre.bind((this));
        this.onChangeEmail = this.onChangeEmail.bind((this));
        this.onChangePassword = this.onChangePassword.bind((this));

        this.onEdit = this.onEdit.bind(this);
        this.usuarioEdit= this.usuarioEdit.bind(this);

        this.state = {
            nombre: '',
            email: '',
            password: ''
        }
    }

    componentDidMount(){
        const {id} = this.props.match.params;
        this.usuarioEdit(id);
    }

    onChangeNombre(evt){
        this.setState({
            nombre: evt.target.value
        })
    }

    onChangeEmail(evt){
        this.setState({
            email: evt.target.value
        });
    }

    onChangePassword(evt){
        this.setState({
            password: evt.target.value
        });
    }

    usuarioEdit(id){

        window.fetch(`http://localhost:4000/api/usuarios/${id}`).then((res) => {
            res.json().then((obj) => {
                this.setState({
                    nombre: obj.nombre,
                    email: obj.email,
                    password: obj.password,
                    id: obj._id
                })
            })
        }).then((res=>res))
    }

    onEdit(evt){
        let id = evt.target.dataset.id

        window.fetch(`http://127.0.0.1:4000/api/usuarios/${id}`, {
            method: 'put',
            body: JSON.stringify({
                nombre: this.state.nombre,
                email: this.state.email,
                password: this.state.password
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then((res) => {
            res.json().then((obj) => {
                console.log(obj)
                if(obj.mensaje === 'OK'){
                    window.location = "http://localhost:3000/";
                }
            })
        })   
    }

    render(){
        return (
            <div>
                <h2>Editar usuario</h2>
                <form onSubmit={this.onEdit} data-id={this.state.id}>
                    <div class="form-group">
                        <label>Nombre</label>
                        <input type="email" class="form-control" type="text" onChange={this.onChangeNombre} value={this.state.nombre}/>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control" type="email" onChange={this.onChangeEmail} value={this.state.email}/>
                    </div>
                    <div class="form-group">
                        <label>Contraseña</label>
                        <input type="password" class="form-control" onChange={this.onChangePassword} value={this.state.password}/>
                    </div>
                    <input type="submit" class="btn btn-primary" value="Editar"/>
                </form>
            </div>
        );
    }
}

export default EditarUsuario;