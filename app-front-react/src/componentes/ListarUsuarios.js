import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Link} from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./tabla.css";


//Al importar el {Component} no hace falta poner React.Component al extender.
class ListarUsuarios extends Component{

    constructor(props){
        super(props);
        this.onDelete = this.onDelete.bind(this);
    }
    //Sirve para montar el componente
    componentDidMount(){
        window.fetch('http://localhost:4000/api/usuarios/').then((res) => {
            res.json().then((obj) => {
                this.setState({
                    listaUsuarios: obj
                })
            })
        })
    }

    componentWillMount(){

    }

    onDelete(evt){
        let id = evt.target.dataset.id;

        window.fetch(`http://127.0.0.1:4000/api/usuarios/${id}`, {
            method: 'delete'
        }).then((res) => {
            res.json().then((obj) => {
                if(obj.mensaje === 'OK'){
                    alert('El usuario se ha borrado con éxito')
                    this.componentDidMount();
                } else {
                    alert('No se ha podido borrar el usuario')
                }
            })
        })
    }

    render(){
        let domVirtual;

        if(this.state === null){
            domVirtual = (<p>Cargando...</p>);
        } else {
            let filas = this.state.listaUsuarios.map((usuario) => {
                return (
                    <tr key={usuario._id}>
                        <td>{usuario.nombre}</td>
                        <td>{usuario.email}</td>
                        <td>
                            <button class="btn btn-danger" onClick={this.onDelete} data-id={usuario._id}>Borrar</button>
                        </td>
                        <td>
                            <Link to={`/editar/${usuario._id}`}>
                                <button class="btn btn-success">Editar</button>
                            </Link>
                            
                        </td>
                    </tr>
                );
            })
            domVirtual = (
                <div>
                    <h2>Lista de usuarios</h2>
                        <div class="tabla">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Nombre</th>
                                        <th scope="col">Email</th>
                                        <th scope="col"></th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {filas}
                                </tbody>
                            </table>
                        </div>
                </div>
            )
        } 
        return domVirtual;
    }
}

export default ListarUsuarios;